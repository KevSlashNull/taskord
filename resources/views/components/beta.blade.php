@if ($background === 'dark')
<span class="badge-font border border-success fw-bold ms-1 px-2 rounded-pill small text-white" title="Feature Release Label: Beta">
    Beta
</span>
@else
<span class="badge-font border border-success fw-bold ms-1 px-2 rounded-pill small text-dark" title="Feature Release Label: Beta">
    Beta
</span>
@endif
