<div class="card" wire:init="loadUser">
    @if (!$readyToLoad)
    <div class="card-body text-center">
        <div class="spinner-border spinner-border-sm taskord-spinner text-secondary" role="status"></div>
    </div>
    @else
    <ul class="list-group list-group-flush">
        <li class="list-group-item text-center py-3">
            <a href="{{ route('user.done', ['username' => $user->username]) }}">
                <img
                    loading=lazy
                    class="rounded-circle avatar-100 mt-1 user-popover"
                    src="{{ Helper::getCDNImage($user->avatar) }}"
                    data-id="{{ $user->id }}"
                    height="40" width="40"
                    alt="{{ $user->username }}'s avatar"
                />
            </a>
            <div class="h4 mt-3">
                @if ($user->firstname or $user->lastname)
                    {{ $user->firstname }}{{ ' '.$user->lastname }}
                @endif
            </div>
            <div class="h5 text-secondary">
                {{ $user->username }}
            </div>
            <div>
                @if ($user->status)
                <div class="d-inline-block border border-1 mt-2 px-2 py-1 rounded-pill">
                    <span>{{ $user->status_emoji }}</span>
                    <span title="{{ $user->status }}">{{ Str::limit($user->status, '50') }}</span>
                </div>
                @endif
            </div>
        </li>
        <li class="list-group-item py-3 fw-bold text-primary">
            {{ $user->followings()->count('id') }} following
            {{ str_plural('user', $user->followings()->count('id')) }}
        </li>
        <li class="list-group-item py-3 fw-bold text-primary">
            {{ $user->tasks()->count('id') }}
            {{ str_plural('task', $user->tasks()->count('id')) }} created
        </li>
    </ul>
    @endif
</div>
