# Release Notes

## Unreleased

### **v1.0.6** (WIP)

- 

## Released

### **v1.0.5** (Jan 10, 2021)

- Now users and products have **Activity Graph** 📊
- Brand new [**Explore**](https://taskord.com/explore) page is beta now 🌎
- **User profile status** is visible everywhere 💭
- [**Open page**](https://taskord.com/open) is now public as a part of open startup 📈
- Users can now see their **account stats** 📊
- **Telegram bot** beta is out now 🤖
- **GitHub and GitLab** commits are now more cleaner ✨
- Added **Who to follow** section to explore active users 👥

### **v1.0.4** (Jan 06, 2021)

- Taskord will load much faster, thanks to **Livewire defer loading** 💨
- Taskord is now a **Progressive web app (PWA)** 🚅
- Icons are now more clear in **dark mode** 🌑
- New task will be created when a product is **launched** 🚀
- ? mark to open **keyboard shortcut dialog** ⌨
- Able to mark a **single notification as read** ✅
- **Bot commits** from GitHub are now skipped 🤖
- **Replies count** is now visible for all questions 1️⃣
- **Modal confirmation** while deleting/resetting your account 🚮
- All images are now served via **Imagekit CDN** 🌐
- Finally, we squashed a lot of **major/minor bugs** 🐛

### **v1.0.3** (Dec 27, 2020)

- Added **Sweetalert 2** for alerts ✅
- Add **Links Prefetch** so the platform feels faster 🚀
- [**Security Logs**](https://taskord.com/settings/logs) for user account 📜
- User reputation points [**log page**](https://taskord.com/reputation) ✨
- Added **keyboard shortcuts** ⌨
- Fix **accessibility** around the site 👁
- **Markdown** support is enhanced 📝
- User can now export their **account logs** 📜
- User can now able to **reset their account** without deleting 🔃
- Added **RSS feed** support for users and products 🔊
- **JQuery** is no more on Taskord 😊

### **v1.0.2** (Dec 20, 2020)

- User can now set **profile status** (still in beta) 💭
- Add Product avatar on the popover 🖼
- Improved **Dark mode**, now it looks even **darker** 🌚
- Improved **sidebar** design 🎨
- **Meetups** is available for all staffs in Taskord, soon to beta 🤝
- Switched Icon system to **Heroicons** from Font Awesome 👀
- Upgraded to **Bootstrap v5.0.0** 🚀

### **v1.0.1** (Nov 29, 2020)

- **Dependencies** are now up to date 📅
- Bumped **Laravel** version to **v8.16.1** 📦
- Bumped **PHP** version to **v7.4.13** 📦
- Logging into Taskord is now faster than before (we switched our session handler to Redis) 💨
- Major **Security** Improvements 🛡

### **v1.0.0** (Oct 21, 2020)

- Added user **profile popover** 💬
- Added product **profile popover** 💬
- **Reputation** is moved from beta to general availability 🌐
- Moved Bootstrap popover to **tippy.js tooltips** ➡️
- Fixed **design** and **performance** issues 🐛
